package test;
import ToysRus_Automation.Storefront_Automation.Functions.Genericfn;
import org.testng.annotations.Test;
import Config.BaseClass;
import ToysRus_Automation.Storefront_Automation.Functions.Loginfn;

public class ToysRusScripts extends BaseClass {
    Loginfn Loginfn = new Loginfn();
    Genericfn Genericfn = new Genericfn();

    @Test
    public void TC001_PurchaseMulPrdtsAsGuest() throws Exception {

        Loginfn.LoginToyRus();   //Login to ToysRus application
        Genericfn.CreateMulOrdersAsGuest(); //Creating multiple orders using guest login
    }




}
