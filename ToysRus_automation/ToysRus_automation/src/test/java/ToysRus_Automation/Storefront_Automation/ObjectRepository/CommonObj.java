package ToysRus_Automation.Storefront_Automation.ObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Config.BaseClass;
import Config.CommonLib;

public class CommonObj {

    public By SignIn = By.xpath("//*[text()='Sign in']");
    public By PrdtIden = By.xpath("(//*[@class='card-title'])");  //*[@data-nimg='intrinsic']
    public By AddBasket = By.xpath("(//button[contains(@title,'Add to basket')])[1]");
    public By AddedConfirm = By.xpath("//*[contains(text(),'added')]");
    public By GoToBasket = By.xpath("//button[contains(text(),'Go To Basket')]");
    public By DeliveryCheck = By.xpath("//span[contains(text(),'delivery in')]");
    public By BagCount = By.xpath("//span[contains(text(),'Bag')]/preceding-sibling::span[1]");
    public By CheckOutBtn = By.xpath("//button[text()='Checkout now']");
    public By EmailField = By.xpath("//*[@name='email']");
    public By ConAsGuest = By.xpath("//button[text()='Continue as guest']");
    public By FirstName = By.xpath("//*[@name='firstName']");
    public By lastName = By.xpath("//*[@name='lastName']");
    public By telephone1 = By.xpath("//*[@name='telephone1']");
    public By city = By.xpath("(//*[@name='city'])[1]/button[1]");
    public By area = By.xpath("(//*[@name='area'])[1]/button[1]");
    public By ContReview = By.xpath("//*[text()='Continue to review & pay']");
    public By PaymentMethod = By.xpath("//*[text()='Payment method']");
    public By SelCity = By.xpath("//*[text()='Dubai']");
    public By SelArea = By.xpath("//*[text()='AL Badaa']");
    public By Address1 = By.name("addressLine1");
    public By PayCredit = By.xpath("//span[text()='Pay by credit / debit card']/preceding::span[1]");
    public By cardHolderName = By.name("cardHolderName");
    public By cardNumber = By.name("cardNumber");
    public By ExpiryMonth = By.xpath("(//*[@name='expiryMonth'])[1]/button[1]");
    public By expiryYear = By.xpath("(//*[@name='expiryYear'])[1]/button[1]");
    public By cardCvn = By.name("cardCvn");
    public By PlaceOrder = By.xpath("(//*[@class='btn btn-success btn-block'])[1]");
    public By OrderNum = By.xpath("//*[text()='Order #']/strong[1]");
    public By SearchPrdt = By.xpath("//*[@class='sr-input']");
    public By NextPage = By.xpath("//*[@class='page-right btn btn-info']");
    public By PageCount= By.xpath("//*[@class='pl-1']");
    public By Prdtfnd= By.xpath("//div[contains(text(),'products found')]");
    public By MapAvailability= By.xpath("//*[text()='Select an Emirate/City']");
    public By ClosePrdt= By.xpath("//button[@label='btn-close']");


    public By HeaderBtn(String Data) {
        By HeaderBtn = By.xpath("//*[contains(@class,'dropdown-toggle nav-link')][text()='" + Data + "']");
        return HeaderBtn;

    }
    public By PrdtSel(int Data) {
        By PrdtSel = By.xpath("(//*[@class='card-title'])["+Data+"]");  //(//*[@data-nimg='intrinsic'])["+Data+"]
        return PrdtSel;

    }
    public By DDFromField(String Data) {
        By DDFromField = By.xpath("//*[text()='"+Data+"']");
        return DDFromField;

    }

}