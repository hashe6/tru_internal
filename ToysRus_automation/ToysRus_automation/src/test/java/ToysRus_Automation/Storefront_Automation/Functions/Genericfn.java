package ToysRus_Automation.Storefront_Automation.Functions;

import Config.BaseClass;
import ToysRus_Automation.Storefront_Automation.ObjectRepository.CommonObj;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Genericfn extends BaseClass {

    CommonObj CommonObj = new CommonObj();

    public void SelectHeaderOption(String HeaderVal) throws Exception {
        Waituntil(CommonObj.HeaderBtn(HeaderVal), 60);
        Element(CommonObj.HeaderBtn(HeaderVal)).click();
    }

    public void AddToCartMulPrdts(String Prdt) throws Exception {
        String RequiredCount = NOP;
        String BagCount = "";
        Waituntil(CommonObj.PrdtIden, 30);
        int PrdtCount = Elements(CommonObj.PrdtIden);

        for (int i = 1; i <= PrdtCount; i++) {
            Waituntil(CommonObj.PrdtSel(i), 30);
            String Prdtname = Element(CommonObj.PrdtSel(i)).getText();
            Element(CommonObj.PrdtSel(i)).click();
            Waituntil(CommonObj.AddBasket, 30);
            if (Elements(CommonObj.DeliveryCheck) > 0) {
                Element(CommonObj.AddBasket).click();
                Waituntil(CommonObj.GoToBasket, 30);
                int AddedConfirmCount = Elements(CommonObj.AddedConfirm);
                Assert.assertNotEquals(AddedConfirmCount, 0);
                Addlog("Pass", i + ". " + Prdtname + " added in the cart", "ProductAdded");
                Element(CommonObj.GoToBasket).click();
                ThinkTime(3);
                BagCount = Element(CommonObj.BagCount).getText();
            }

            if (BagCount.equals(RequiredCount)) {
                break;
            }
            SelectHeaderOption(Prdt);
        }

    }

    public void CheckOutFn(int val) throws Exception {
       /* Waituntil(CommonObj.BagCount, 30);
        Element(CommonObj.BagCount).click();*/
        ThinkTime(2);
        Waituntil(CommonObj.CheckOutBtn, 30);
        Element(CommonObj.CheckOutBtn).click();
        ThinkTime(3);
        Waituntil(CommonObj.EmailField, 30);
        Element(CommonObj.EmailField).sendKeys("random" + val + "@gmail.com");
        Element(CommonObj.ConAsGuest).click();

        ExceptionHandlingInCart();
        Waituntil(CommonObj.FirstName, 30);
        Element(CommonObj.FirstName).sendKeys("test");
        Element(CommonObj.lastName).sendKeys("test");
        Element(CommonObj.telephone1).sendKeys("587098993");
        Waituntil(CommonObj.city, 30);
        Element(CommonObj.city).click();
        Waituntil(CommonObj.DDFromField("Dubai"), 30);
        Element(CommonObj.DDFromField("Dubai")).click();
        Waituntil(CommonObj.area, 30);
        Element(CommonObj.area).click();
        Waituntil(CommonObj.DDFromField("AL Badaa"), 30);
        Element(CommonObj.DDFromField("AL Badaa")).click();
        Waituntil(CommonObj.Address1, 30);
        Element(CommonObj.Address1).sendKeys("Dubai street");
        Addlog("Pass", "Checkout details entered", "Details Added");
        Waituntil(CommonObj.ContReview, 30);
        Element(CommonObj.ContReview).click();
        Waituntil(CommonObj.PaymentMethod, 30);

    }

    public String PaymentAndPlaceOrder() throws Exception {
        Waituntil(CommonObj.PayCredit, 30);
        Element(CommonObj.PayCredit).click();
        Waituntil(CommonObj.cardHolderName, 30);
        Element(CommonObj.cardHolderName).sendKeys("Test Test");
        Waituntil(CommonObj.cardNumber, 30);
        Element(CommonObj.cardNumber).sendKeys("4111 1111 1111 1111");
        Waituntil(CommonObj.ExpiryMonth, 30);
        Element(CommonObj.ExpiryMonth).click();
        Waituntil(CommonObj.DDFromField("01"), 30);
        Element(CommonObj.DDFromField("01")).click();
        Waituntil(CommonObj.expiryYear, 30);
        Element(CommonObj.expiryYear).click();
        Waituntil(CommonObj.DDFromField("2022"), 30);
        Element(CommonObj.DDFromField("2022")).click();
        Waituntil(CommonObj.cardCvn, 30);
        Element(CommonObj.cardCvn).sendKeys("345");
        Addlog("Pass", "Card Details Entered", "Card Details");
        Waituntil(CommonObj.PlaceOrder, 30);
        Element(CommonObj.PlaceOrder).click();
        Waituntil(CommonObj.OrderNum, 30);
        String OrderNumber = Element(CommonObj.OrderNum).getText();
        Addlog("Pass", "Order created with Order Num: " + OrderNumber, "Order created");
        return OrderNumber;
    }

    public void CreateMulOrdersAsGuest() throws Exception {

        List list = GetCountofProducts();
        int Slno = 0;
        String ProductName = "", OrderNumber;
        for (int m = 0; m < list.size(); m++) {
            OrderNumber = "Order failed";
            driver.get(uRL1);
            ProductName = list.get(m).toString();
            try {

                Waituntil(CommonObj.SearchPrdt, 30);
                Element(CommonObj.SearchPrdt).sendKeys(ProductName);
                Element(CommonObj.SearchPrdt).sendKeys(Keys.ENTER);
                ThinkTime(3);
                if (Elements(CommonObj.Prdtfnd) > 0) {
                    Waituntil(CommonObj.PrdtSel(1), 30);
                    Element(CommonObj.PrdtSel(1)).click();
                }
                ThinkTime(2);
                Waituntil(CommonObj.AddBasket, 30);
                ThinkTime(2);
                if (Elements(CommonObj.DeliveryCheck) > 0) {
                    Element(CommonObj.AddBasket).click();
                    Waituntil(CommonObj.GoToBasket, 30);
                    int AddedConfirmCount = Elements(CommonObj.AddedConfirm);
                    Assert.assertNotEquals(AddedConfirmCount, 0);
                    Addlog("Pass", m + 1 + ". " + ProductName + " added in the cart", "ProductAdded");
                    Element(CommonObj.GoToBasket).click();
                    ThinkTime(3);
                    CheckOutFn(m + 1);
                    OrderNumber = PaymentAndPlaceOrder();


                }else {
                    Addlog("fail", m + 1 + ". " + ProductName + " not added in the cart since the product is not having delivery option", "ProductNotAdded");
                }
            } catch (Exception e) {
                Addlog("Fail", m + 1 + ". Unable to add " + ProductName, "ProductNotAdded");
                driver.get(uRL1);
            }
            Slno = OrderIdtoExcel(Slno, OrderNumber, ProductName);

        }
        if(MailRequired.equals("Yes")){
        mail();}
    }

    public List<String> GetCountofProducts() throws Exception {
        int NOO = Integer.parseInt(NOP);
        int PrdtCount = Elements(CommonObj.PrdtIden);
        List<String> list = new ArrayList<>();
        Waituntil(CommonObj.PrdtIden, 30);
        String PageCountStr = Element(CommonObj.PageCount).getText();
        int PageCount = Integer.parseInt(PageCountStr);
        for (int i = 1; i <= PageCount; i++) {
            for (int j = 1; j <= PrdtCount; j++) {
                String Prdtname = Element(CommonObj.PrdtSel(j)).getText();
                list.add(Prdtname);
                if (list.size() >= NOO) {
                    break;
                }
            }
            if (list.size() >= NOO) {
                break;
            }
            Waituntil(CommonObj.NextPage, 30);
            Element(CommonObj.NextPage).click();
        }
        Addlog("Pass", "Total number of products added into the list is " + list.size(), "Test");
        return list;
    }

    public int OrderIdtoExcel(int Slno, String OrderId, String ProductName) throws Exception {
        String path;
        FileInputStream fis;
        XSSFWorkbook workbook;
        XSSFSheet sheet;
        Slno++;
        int r = Slno;
        try {
            path = System.getProperty("user.dir") + "\\Results\\Datasheet.xlsx";
            fis = new FileInputStream(path);
            workbook = new XSSFWorkbook(fis);
            sheet = workbook.getSheetAt(0);
            Row row = sheet.createRow(r);
            Cell cell1update = row.createCell(0);
            cell1update.setCellValue(Slno);
            System.out.println("Enter");
            Cell cell2update = row.createCell(1);
            cell2update.setCellValue(ProductName);
            Cell cell3update = row.createCell(2);
            cell3update.setCellValue(OrderId);
            Cell cell4update = row.createCell(3);
            cell4update.setCellValue(randomid);
            Cell cell5update = row.createCell(4);
            cell5update.setCellValue(Browser);
            fis.close();
            FileOutputStream output = new FileOutputStream(path);
            workbook.write(output);
            output.close();
            System.out.println("Entered to Excel");

        } catch (Exception e) {
            System.out.println(e);
            Slno--;
        }
        return Slno;

    }
    //Doing Exception handling here
    public void ExceptionHandlingInCart() throws  Exception
    {
        ThinkTime(3);
        if(Elements(CommonObj.MapAvailability) > 0) {
            Addlog("Fail","Stock not available","no Stock");
            driver.get(uRL1);
            ThinkTime(2);
            Waituntil(CommonObj.BagCount,30);
            Element(CommonObj.BagCount).click();
            ThinkTime(2);
            Waituntil(CommonObj.ClosePrdt,30);
            Element(CommonObj.ClosePrdt).click();
        }

    }
}



