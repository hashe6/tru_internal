package Config;

import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.annotations.BeforeMethod;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import java.text.SimpleDateFormat;
import javax.mail.*;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.lang.reflect.Method;

import org.testng.ITestResult;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class BaseClass {

    public static WebDriver driver;
    public static ExtentTest test;
    public static ExtentReports report;
    public static String testName;
    static int i = 0;
    public String Env = CommonLib.readUrlsPropertyFile("Environment");
    public String uRL = CommonLib.readUrlsPropertyFile(Env);
    public String Browser = CommonLib.readUrlsPropertyFile("Browser");
    public String NOP = CommonLib.readUrlsPropertyFile("NumberOfOrders");
    public static String MailTo = CommonLib.readUrlsPropertyFile("MailTo");
    public static String MailRequired = CommonLib.readUrlsPropertyFile("MailRequired");
    public String Headless = CommonLib.readUrlsPropertyFile("Headless");
    public static SimpleDateFormat sdf;
    public static String ReportTime;
    String s1 = uRL.substring(uRL.indexOf("@") + 1);
    public String uRL1 = "https://" + s1.trim();
    public static Random random = new Random();
    public static int randomid;

    public static void clickable(By xpath1) throws Exception {

        By xpath2 = xpath1;

        WebDriverWait wait = new WebDriverWait(driver, 20);

        WebElement element = driver.findElement(xpath2);

        if (element.isDisplayed() && element.isEnabled()) {
            wait.until(ExpectedConditions.elementToBeClickable(xpath2));
            System.out.println("Element is enabled and clickable");

        } else {
            fullpage_screenshot(xpath2.toString());
        }

    }

    public void imagepresent(By xpath1) throws Exception {

        System.out.println("Logo clicked");

        By xpath2 = xpath1;

        try {
            CommonLib.waitForPageLoad(driver);
            WebElement ImageFile = driver.findElement(xpath2);
            Boolean ImagePresent = (Boolean) ((JavascriptExecutor) driver).executeScript(
                    "return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0",
                    ImageFile);

            if (!ImagePresent) {
                System.out.println("Image not displayed.");

                Assert.assertNull(ImageFile);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void fullpage_screenshot(String name) throws Exception {
        i = i + 1;
        String Testcase = name;
        Screenshot s = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver);
        File directory = new File("./screenshot");
        if (!directory.exists()) {
            directory.mkdirs();
        }
        ImageIO.write(s.getImage(), "PNG", new File("./screenshot//" + name + ".png"));
        System.out.println("Screenshot taken");
    }

    public static String separatorsToSystem(String path) {

        if (path == null) {
            return null;
        }
        if (SystemUtils.IS_OS_LINUX) {

            return FilenameUtils.separatorsToUnix(path);
        } else {
            return FilenameUtils.separatorsToWindows(path);
        }
    }

    public void pageAvailability(By xpath) throws Exception {
        boolean display = driver.findElement(xpath).isDisplayed();
        By xpath1 = xpath;
        WebDriverWait wait = new WebDriverWait(driver, 50);
        if (display) {
            wait.until(ExpectedConditions.elementToBeClickable(xpath1));
            driver.findElement(xpath1).click();
            String url = driver.getCurrentUrl();
            String title = driver.getTitle();
            //System.out.println(title);
            Thread.sleep(3000);
            Assert.assertNotEquals(url, "https://www.aceuae.com/404/");
            Assert.assertNotEquals(url, "https://www.aceuae.com/en-ae/###");
            Assert.assertNotEquals(title, "404 | ACE UAE");
        } else {
            System.out.println("element not present");
            fullpage_screenshot("Page not available");
        }
    }

    @BeforeClass
    public static void random() {
        sdf = new SimpleDateFormat("dd-MMM-YY_HH.mm.ss");
        ReportTime = sdf.format(System.currentTimeMillis());
    }

    @BeforeMethod
    public void beforeMethod(Method method) throws Exception {
        randomid = random.nextInt(500000);
        if (Browser.equals("Chrome")) {
            if (System.getProperty("webdriver.chrome.driver") != null) {
                ChromeOptions options = new ChromeOptions();
                if(Headless.equals("True")) {
                    options.setHeadless(true);
                }
                else
                {
                    options.setHeadless(false);
                }

                driver = new ChromeDriver(options);
            } else if (System.getProperty("phantomjs_binary_path") != null) {
                DesiredCapabilities caps = new DesiredCapabilities();
                caps.setJavascriptEnabled(true);
                caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
                        System.getProperty("phantomjs_binary_path"));
                driver = new PhantomJSDriver(caps);
            }
        } else if (Browser.equals("Edge")) {
            System.setProperty("webdriver.edge.driver", System.getProperty("user.dir") + "/resources/msedgedriver.exe");
            driver = new EdgeDriver();

        } else if (Browser.equals("Firefox")) {
            System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/resources/geckodriver.exe");
            driver = new FirefoxDriver();

        } else {
            throw new RuntimeException("Unknown web driver specified.");
        }

        driver.manage().window().maximize();

        testName = method.getName();
        startTest(testName);

    }

    @AfterMethod
    public void afterMethod(ITestResult result) throws Exception {

        if (result.getStatus() == ITestResult.FAILURE) {
            Throwable testResult = result.getThrowable();
            System.out.println(testResult);
            Addlog("fail", "Test case Failed- " + testResult, testName + "_Failed");
        }

        driver.quit();

        report.endTest(test);
        report.flush();
        report.close();

    }


    public static void Waituntil(By Obj, int time) throws Exception {
        WebDriverWait wait = new WebDriverWait(driver, time);
        //ThinkTime(2);
        wait.until(ExpectedConditions.elementToBeClickable(Obj));

    }

    public static WebElement Waituntil1(By Obj, int time) throws Exception {
        if (time > 0) {
            ThinkTime(2);
            WebDriverWait wait = new WebDriverWait(driver, time);
            return wait.until(ExpectedConditions.elementToBeClickable(Obj));
        }
        return driver.findElement(Obj);
    }

    public static WebElement Element(By Obj) {
        WebElement element = driver.findElement(Obj);
        return element;
    }

    public static int Elements(By Obj) {
        int elements = driver.findElements(Obj).size();
        return elements;
    }

    public static void ThinkTime(long wait) throws Exception {
        Thread.sleep(wait * 1000);
    }

    public static void startTest(String testname) {

        report = new ExtentReports(System.getProperty("user.dir") + "/resources/Reports/" + ReportTime + "_run results" + "/Results.html", false);
        test = report.startTest(testname);

    }

    public static void Addlog(String Out, String Desc, String filename) throws Exception {

        if (Out.equalsIgnoreCase("pass")) {
            ThinkTime(1);
            test.log(LogStatus.PASS, Desc, test.addScreenCapture(capture(testName, filename)));
        }
        if (Out.equalsIgnoreCase("fail")) {
            ThinkTime(1);
            test.log(LogStatus.FAIL, Desc, test.addScreenCapture(capture(testName, filename)));
        }
        if (Out.equalsIgnoreCase("Info")) {
            ThinkTime(1);
            test.log(LogStatus.INFO, Desc, test.addScreenCapture(capture(testName, filename)));
        }
    }

    public static String capture(String testName, String filename) throws IOException {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File Dest = new File(System.getProperty("user.dir") + "/resources/Reports/" + ReportTime + "_run results" + "/" + testName + "/" + System.currentTimeMillis() + ".png");
        String errflpath = Dest.getAbsolutePath();
        FileUtils.copyFile(scrFile, Dest);
        return errflpath;
    }

    public static void mail() {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");
        Session session = Session.getDefaultInstance(props,

                new javax.mail.Authenticator() {

                    protected PasswordAuthentication getPasswordAuthentication() {

                        return new PasswordAuthentication("hasheer.as@mozanta.com", "Richu@123");

                    }

                });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("hasheer.as@mozanta.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(MailTo));
            message.setSubject("Automation || Order Details");
            BodyPart messageBodyPart1 = new MimeBodyPart();
            messageBodyPart1.setText("Please find the attached recently created orders through automation");

            // Create another object to add another content
            MimeBodyPart messageBodyPart2 = new MimeBodyPart();

            // Mention the file which you want to send
            String filename = System.getProperty("user.dir") + "\\Results\\Datasheet.xlsx";

            // Create data source and pass the filename
            DataSource source = new FileDataSource(filename);

            // set the handler
            messageBodyPart2.setDataHandler(new DataHandler(source));

            // set the file
            messageBodyPart2.setFileName(filename);

            // Create object of MimeMultipart class
            Multipart multipart = new MimeMultipart();

            // add body part 1
            multipart.addBodyPart(messageBodyPart2);

            // add body part 2
            multipart.addBodyPart(messageBodyPart1);

            // set the content
            message.setContent(multipart);

            // finally send the email
            Transport.send(message);



        } catch (MessagingException e) {

            throw new RuntimeException(e);

        }

    }

}







