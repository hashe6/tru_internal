package Config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonLib extends BaseClass {

    static WebDriver driver;

    // Reading from property file contains the webElements
    public static String readElementPropertyFile(String element) {
        Properties prop = new Properties();


        String url1 = System.getProperty("user.dir") + "/resources/OR.properties";

        String url = separatorsToSystem(url1);


        File file = new File(url);
        String elementValue = "";
        FileInputStream fileInput = null;
        try {
            fileInput = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            prop.load(fileInput);
            elementValue = prop.getProperty(element);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return elementValue;
    }

    // Reading from property file contains the data values
    public static String readUrlsPropertyFile(String data) {
        Properties prop = new Properties();
        String url = System.getProperty("user.dir") + "\\resources\\Config.properties";
        File file = new File(url);
        String dataValue = "";
        FileInputStream fileInput = null;
        try {
            fileInput = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            prop.load(fileInput);
            dataValue = prop.getProperty(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dataValue;
    }

    // Wait for page load
    public static void waitForPageLoad(WebDriver driver) {
        ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
            }
        };
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(pageLoadCondition);
    }

    // Wait
    public static WebDriverWait getWait() {
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.pollingEvery(250, TimeUnit.MILLISECONDS);
        wait.ignoring(NoSuchElementException.class);
        return wait;
    }

    // sleep
    public static void handleSleep(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // scroll
    public static void scrollDown(String diamentionToScroll) {
        JavascriptExecutor scroll = (JavascriptExecutor) driver;
        scroll.executeScript(diamentionToScroll);
    }

    public static void scroll_Down() {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    // checking is clickable
    public static WebElement waitForElementToBeClickable(WebDriver driver, WebElement elementToBeLoaded, int Time) {
        WebDriverWait wait = new WebDriverWait(driver, Time);
        WebElement Element = wait.until(ExpectedConditions.elementToBeClickable(elementToBeLoaded));
        return Element;
    }

    // waiting for element to load
    public static WebElement isElementLoaded(WebDriver driver, WebElement elementToBeLoaded, int Time) {
        WebDriverWait wait = new WebDriverWait(driver, Time);
        WebElement Element = wait.until(ExpectedConditions.visibilityOf(elementToBeLoaded));
        return Element;
    }


}
